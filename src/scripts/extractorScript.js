function cleanLink(link) {
	if(!link) return link;
	return link.split("&opi")[0].replace(/^\/url\?q=/, "");
}


function getLastKey(data) {
	const keys = Object.keys(data);
	return keys[keys.length - 1];
}


function safeGet(data, keys) {
	keys.forEach(key => {
		try{
			if(key === -1) key = getLastKey(data);
			data = data[key];
		}catch(error) {
			return null;
		}
	});
	return data;
}

function getCategories(data) {
	return safeGet(data, [14, 13]);
}

function getNumber(data) {
	return safeGet(data, [14, 178, 0, 0]);
}

function getPlaceId(data) {
	return safeGet(data, [14, 0]);
}


function getRating(data) {
	return safeGet(data, [14, 4, 7]);
}

function getReviews(data) {
	return safeGet(data, [14, 4, 3, 1]);
}

function getPriceRange(data) {
	const price =  safeGet(data, [6, 4, 2]);
	if (price) {
		return "$".repeat(price.length);
	}
	return null;
}

function getTitle(data) {
	return safeGet(data, [14, 11]);
}

function getAddress(data) {
	return safeGet(data, [14, 39]);
}

function getPlace(data) {
	return (safeGet(data, [14, 39])?.split(",")[1]) || "";
}

function getWebsite(data) {
	return cleanLink(safeGet(data, [6, 7, 0]));
}

function getCategorie(data) {
	return safeGet(data, [6, 13, 0]);
}

function parse(inputString) {

	if(inputString.startsWith(")]}'")){
		inputString = inputString.substring(4);
	}
	return JSON.parse(inputString);
}


function extractData(inputString, link) {
	const arrayPlaces = parse(inputString)[0][1];
	const extractedData = arrayPlaces.map(place => {
		return {
			"place_id": getPlaceId(place),
			"name": getTitle(place),
			"link": link,
			"main_category": getCategorie(place),
			"categories": getCategories(place),
			"rating": getRating(place),
			"reviews":getReviews(place),
			"address": getAddress(place),
			"website": getWebsite(place),
			"price": getPriceRange(place),
			"place": getPlace(place),
			"phone": getNumber(place)
		};
	});

	return extractedData;
}


const extractor = {
	extractData
};

module.exports =  extractor;
