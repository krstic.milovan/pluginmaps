const CopyPlugin = require("copy-webpack-plugin");
const HtmlPlugin = require("html-webpack-plugin");
const path =  require("path");

module.exports = {
	mode: "development",
	devtool: "cheap-module-source-map",
	entry: {
		popup: path.resolve("./src/popup/index.tsx"),
		contentScript: path.resolve("./src/scripts/contentScript.js"),
		injected: path.resolve("./src/scripts/injected.js"),
		extractorScript: path.resolve("./src/scripts/extractorScript.js"),
	},
	module: {
		rules: [
			{
				use: "ts-loader",
				test: /\.tsx$/,
				exclude: /node_modules/
			},
			{
				use: ["style-loader",
					{
						loader: "css-loader",
						options: {
							importLoaders: 1,
						},
					}
				],
				test: /\.css$/i,
			},
			{
				type: "static/",
				test: /\.(png|jpg|jpeg|gif|woff|woff2|tff|eot|svg)$/,
			},
			{
				test: require.resolve("./src/scripts/extractorScript.js"),
				use: {
					loader: "expose-loader",
					options: {
						exposes: "extractor",
					},
				}
			}
		]
	},
	plugins: [
		new CopyPlugin({
			patterns: [
				{from: path.resolve("src/assets/manifest.json"), to: path.resolve("dist")},
				{from: path.resolve("src/assets/images/icon.png"), to: path.resolve("dist")},
			],
		}),
		new CopyPlugin({
			patterns: [{
				from: path.resolve("src/static"),
				to: path.resolve("dist")
			}]
		}),
		...getHtmlPlugins([
			"popup"
		])
	],
	resolve: {
		extensions: [".tsx", ".ts", ".js"],
		fallback: {
			"path": require.resolve("path-browserify"),
			"got": require.resolve("got")
		}
	},
	output: {
		filename: "[name].js",
		path: path.join(__dirname, "dist")
	},
	optimization: {
		splitChunks: {
			chunks: "all",
		}
	}
};


function getHtmlPlugins(chunks) {
	return chunks.map(chunk => new HtmlPlugin({
		title: "React Extension",
		filename: `${chunk}.html`,
		chunks: [chunk]
	}));
}
