import React from "react";
import {createRoot} from "react-dom/client";
import Popup from "./popup";


function onClickHandler(event) {
	console.log(1);
}


function init() {

const container = document.createElement("div");
document.body.appendChild(container);
if(!container) {
	throw new Error("Can not find AppContainer");
}

const root = createRoot(container);
root.render(<Popup />);
}

init();
