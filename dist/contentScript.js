/******/ (() => { // webpackBootstrap
var __webpack_exports__ = {};
/*!**************************************!*\
  !*** ./src/scripts/contentScript.js ***!
  \**************************************/
let count = 0;

const listener = function(event) {
	if (event.data.type === "xhr") {
		try {
			const response =(event.data.data).response;
			const data = extractor.extractData(response.trim(), (event.data.data).url);
			count += data.length;
			document.getElementsByClassName("s-progress")[0].textContent = `Extracting ${count} leads.\nPlease wait a momnet.`;
		} catch (error) {
			console.error("Error parsing XHR response:", error);
		}
	}
};



// constructor for elements
function registerAnimations() {

	const style = document.createElement("style");
	style.textContent = `
		@keyframes rotateAnimation {
			from {
				transform: rotate(0deg);
			}
			to {
				transform: rotate(360deg);
			}
		}
	`;
	document.head.appendChild(style);
}

function addStyles(tag, styles) {
	Object.keys(styles).forEach(key => {
		tag.style[key] = styles[key];
	});
}

function addEvent(tag, events) {
	Object.keys(events).forEach(key => {
		tag.addEventListener(key, events[key]);
	});
}

function addText(tag, text) {
	tag.textContent = text;
}

function addClass(tag, name) {
	tag.classList.add(name);
}

function createElement({styles = {}, events = {}, text = "", cname = ""}, tag) {
	const element = document.createElement(tag);
	addStyles(element, styles);
	addText(element, text);
	addEvent(element, events);
	if(cname) {
		addClass(element, cname);
	}
	return element;
}

let scrollIntervalId;
let timer;
let timeout;

//TODO: find better whey then timeout function

const scrollFunction = function() {
	return new Promise((resolve, reject) => {
		// wait for content to be loaded in scrollable element
		timeout = setTimeout(() => {
			const resultsContainer = document.querySelector("[role='feed']");
			if(!resultsContainer) reject();
			//scroll speed info
			const scrollSpeed = 40;
			const scrollInterval = 2000;
			const scrollAmount = scrollSpeed * scrollInterval;
			const scrollDirection = 1;
			let lastScrollTop = resultsContainer.scrollTop;

			scrollIntervalId = setInterval(() => {
				const currentScrollTop = resultsContainer.scrollTop;
				if (currentScrollTop !== lastScrollTop) {
					clearTimeout(timer);
					//handle for reaching the end of scroll or infinite load
					timer = setTimeout(() => {
						clearInterval(scrollIntervalId);
						resolve();
					}, 5000);
				}

				resultsContainer.scrollTop += scrollDirection * scrollAmount;
				lastScrollTop = currentScrollTop;
			}, scrollInterval);

		}, 5000);
	});
};



function initMenu(){
	// style sheets for elements
	const injectElementCss = {
		styles: {
			position: "absolute",
			top: "100px",
			left: "500px",
			zIndex: "100",
			background: "#FFFAF9",
			padding: "1rem",
			margin: "1rem",
			borderRadius: "1rem",
			width: "300px",
			height: "60px",
			boxShadow: "12px 12px 2px 1px rgba(0, 0, 40, .2)"
		}
	};

	const extractButtonCss = {
		styles: {
			backgroundColor: "#B6A2D1",
			color: "white",
			width: "300px",
			height: "40px",
			borderWidth: "1px",
			textAlign: "center",
			paddingBottom: "1px",
			border: "1px solid #2C3539",
			borderRadius: "10px",
			cursor: "pointer"
		},
		events: {
			"mouseover": function() {
				extractButton.style.backgroundColor = "#B6A2EF";
				extractButton.style.transition = "background 1s";
			},
			"mouseout": function() {
				extractButton.style.backgroundColor = "#B6A2D1";
			},
			"click": function() {
				// setting up event listener to restive data from injection script
				window.addEventListener("message", listener, true);

				searchButton.click();
				const value = document.getElementById("searchboxinput").value || "";

				if(value === "") {
					window.removeEventListener("message", listener, true);
					return 0;
				}
				scrollFunction().then(() => {
					count = 0;
					document.getElementsByClassName("s-progress")[0].textContent = `Extracting ${count} leads.\nPlease wait a momnet.`;
					buttonStop.style.display = "none";
					statusDiv.style.display = "none";
					extractButton.style.display = "block";
					loader.style.animation = "none";
					injectElement.style.height = "60px";
				});

				extractButton.style.display = "none";
				buttonStop.style.display = "block";
				statusDiv.style.display = "flex";
				statusDiv.style.alignItems = "ceneter";
				injectElement.style.height = "110px";
				loader.style.animation = "rotateAnimation 2s infinite linear";
			}
		},
		text: "Start Scraping"
	};


	const stopButtonCss = {
		styles: {
			backgroundColor: "#FF8E8E",
			color: "black",
			width: "300px",
			height: "40px",
			borderWidth: "1px",
			textAlign: "center",
			paddingBottom: "1px",
			borderRadius: "10px",
			cursor: "pointer",
			display: "none",
			border: "1px solid #2C3539"
		},
		events: {
			"mouseover": function() {
				buttonStop.style.backgroundColor = "#F66E6E";
				buttonStop.style.transition = "background 1s";
			},
			"mouseout": function() {
				buttonStop.style.backgroundColor = "#FF8E8E";
			},
			"click": function() {
				clearInterval(scrollIntervalId);
				clearInterval(timer);
				if(timeout) {
					clearTimeout(timeout);
				}
				window.removeEventListener("message", listener, true);
				count = 0;
				document.getElementsByClassName("s-progress")[0].textContent = `Extracting ${count} leads.\nPlease wait a momnet.`;
				buttonStop.style.display = "none";
				statusDiv.style.display = "none";
				extractButton.style.display = "block";
				loader.style.animation = "none";
				injectElement.style.height = "60px";
			}
		},
		text:"Stop"
	};

	const statusDivCss = {
		styles: {
			width: "300px",
			height: "50px",
			display: "none",
			alignItems: "center"
		}
	};

	const hrCss = {
		styles:{
			border: "0",
			height: "2px",
			position: "relative",
			backgroundImage: "radial-gradient(#808080, #fff)",
		}
	};


	const footerSpanCss = {
		styles: {
			color: "#777",
			fontSize: "0.8rem",
			display: "block",
			textAlign: "center"
		},
		text: "Powered by Constel v0.0.0.1"
	};

	const progressSpanCss ={
		styles: {
			marginLeft: "10px",
			fontSize: "15px"
		},
		text: "Extracting 0 leads.\nPlease wait a momnet.",
		cname: "s-progress"
	};

	//initialize animation
	registerAnimations();

	// Prepare search element
	const searchButton = document.getElementById("searchbox-searchbutton");

	/// create elements
	const injectElement = createElement(injectElementCss, "div");
	const extractButton = createElement(extractButtonCss,"button");
	const buttonStop = createElement(stopButtonCss, "button");
	const statusDiv = createElement(statusDivCss, "div");
	const hr = createElement(hrCss, "hr");
	const footerSpan = createElement(footerSpanCss, "span");
	const progressSpan = createElement(progressSpanCss, "span");

	const loader = document.createElement("img");
	loader.src = chrome.runtime.getURL("./loaderAlt.svg");
	loader.style.width = "30px";
	loader.style.height = "30px";
	loader.style.transition = "transform 1s";


	document.body.appendChild(injectElement);
	injectElement.appendChild(extractButton);
	statusDiv.appendChild(loader);
	statusDiv.appendChild(progressSpan);
	injectElement.appendChild(statusDiv);
	injectElement.appendChild(buttonStop);
	injectElement.appendChild(hr);
	injectElement.appendChild(footerSpan);
}


const initInjection = function() {

	const script = document.createElement("script");
	const containerDiv = document.createElement("div");

	//inject our injection script into script element
	script.src = chrome.runtime.getURL("./injected.js");

	script.onload = function() {
		console.log("Injected script loaded");
		this.remove();
	};

	script.onerror = function() {
		console.error("Failed to load injected script");
	};

	(document.head || document.documentElement).appendChild(script);

	document.addEventListener("DOMContentLoaded", function() {
		containerDiv.id = "container";
		document.body.appendChild(containerDiv);

		fetch(chrome.runtime.getURL("../views/index.html"))
			.then(response => response.text())
			.then(html => {
				containerDiv.innerHTML = html;
			})
			.catch(error => {
				console.error("Failed to load HTML file: ", error);
			});

	});

};


initMenu();
initInjection();



/******/ })()
;
//# sourceMappingURL=contentScript.js.map