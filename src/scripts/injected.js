(function(xhr) {
	var XHR = XMLHttpRequest.prototype;
	var open = XHR.open;
	var send = XHR.send;

	XHR.open = function(method, url) {
		this._method = method;
		this._url = url;
		return open.apply(this, arguments);
	};

	XHR.send = function(postData) {
		var self = this;
		var onload = function() {
			if (self.getResponseHeader("Content-Type") && self.getResponseHeader("Content-Type").includes("application/json")) {
				if(self._url.includes("/search?tbm=map")){
					var responseData = {
						url: self._url,
						method: self._method,
						response: JSON.parse(self.response.slice(0, -6)).d
					};
					window.postMessage({ type: "xhr", data: responseData }, "*");
				}
			}
		};
		this.addEventListener("load", onload);
		return send.apply(this, arguments);
	};
})(XMLHttpRequest);




